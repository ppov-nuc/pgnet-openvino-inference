from openvino.runtime import Core
from ppocr.data import create_operators, transform
from ppocr.postprocess import build_post_process
import cv2
import numpy as np
import time

# 指定PGNet模型路径
pgnet_path = "./e2e_server_pgnetA_infer/inference.pdmodel"

# 创建Core对象
core = Core()
# 载入并编译PGNet模型
pgnet = core.compile_model(model=pgnet_path, device_name="CPU")

# 创建preprocess_op
pre_process_list = [{
            'E2EResizeForTest': {
        'max_side_len': 768,
        'valid_set': 'totaltext'}
        }, {
            'NormalizeImage': {
                'std': [0.229, 0.224, 0.225],
                'mean': [0.485, 0.456, 0.406],
                'scale': '1./255.',
                'order': 'hwc'
            }
        }, {
            'ToCHWImage': None
        }, {
            'KeepKeys': {
                'keep_keys': ['image', 'shape']
            }
        }]
preprocess_op = create_operators(pre_process_list)

# 创建postprocess_op
postprocess_params = {}
postprocess_params['name'] = 'PGPostProcess'
postprocess_params["score_thresh"] = 0.5
postprocess_params["character_dict_path"] = "./ic15_dict.txt"
postprocess_params["valid_set"] = 'totaltext'
postprocess_params["mode"] = 'fast'
postprocess_op = build_post_process(postprocess_params)

def clip_det_res(points, img_height, img_width):
    for pno in range(points.shape[0]):
        points[pno, 0] = int(min(max(points[pno, 0], 0), img_width - 1))
        points[pno, 1] = int(min(max(points[pno, 1], 0), img_height - 1))
    return points
# 定义filter_tag_det_res_only_clip函数
def filter_tag_det_res_only_clip(dt_boxes, image_shape):
        img_height, img_width = image_shape[0:2]
        dt_boxes_new = []
        for box in dt_boxes:
            box = clip_det_res(box, img_height, img_width)
            dt_boxes_new.append(box)
        dt_boxes = np.array(dt_boxes_new)
        return dt_boxes

# 载入图像数据并实现预处理
image_path = 'img623.jpg'
image = cv2.imread(image_path)
data = {'image': image}
data = transform(data, preprocess_op)
img, shape_list = data
img = np.expand_dims(img, axis=0)
shape_list = np.expand_dims(shape_list, axis=0)
starttime = time.time()

# Do the inference by OpenVINO
outputs = pgnet([img])
out_layers = pgnet.output
preds = {}
preds['f_border'] = outputs[out_layers(0)]
preds['f_char'] = outputs[out_layers(1)]
preds['f_direction'] = outputs[out_layers(2)]
preds['f_score'] = outputs[out_layers(3)]

post_result = postprocess_op(preds, shape_list)
points, strs = post_result['points'], post_result['texts']
dt_boxes = filter_tag_det_res_only_clip(points, image.shape)
elapse = time.time() - starttime

print(f"Predict time: {elapse}s")
import utility
src_im = utility.draw_e2e_res(points, strs, image_path)
cv2.imshow("PGNet infer by OpenVINO", src_im)
cv2.waitKey(0)
cv2.destroyAllWindows()